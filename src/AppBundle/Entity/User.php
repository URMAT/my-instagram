<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter your name.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    private $author;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Post", mappedBy="author_user_id")
     */
    private $post;

//    /**
//     * @var ArrayCollection
//     *
//     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Post", mappedBy="likers")
//     */
//    private $likedPosts;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="follows")
     */
    private $followers;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="followers")
     * @ORM\JoinTable(name="followers",
     *     joinColumns={@ORM\JoinColumn(name="follower_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="followed_id", referencedColumnName="id")}
     *     )
     */
    private $follows;

    public function __construct()
    {
        parent::__construct();
        $this->post = new ArrayCollection();
//        $this->likedPosts = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->follows = new ArrayCollection();
    }

    /**
     * @param string $author
     * @return User
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return ArrayCollection
     */
    public function getPost()
    {
        return $this->post;
    }

//    /**
//     * @param Post $post
//     */
//    public function removeLikedPosts(Post $post)
//    {
//        $this->likedPosts->removeElement($post);
//    }

//    /**
//     * @param Post $post
//     */
//    public function addLikedPosts(Post $post)
//    {
//        $this->likedPosts->add($post);
//    }
//
//    /**
//     * @return ArrayCollection
//     */
//    public function getLikedPosts()
//    {
//        return $this->likedPosts;
//    }

    /**
     * @return mixed
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @return mixed
     */
    public function getFollows()
    {
        return $this->follows;
    }

    /**
     * @param User $user
     */
    public function addFollowers(User $user)
    {
        $this->followers->add($user);
    }

    /**
     * @param User $user
     */
    public function addFollows(User $user)
    {
        $this->follows->add($user);
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function hasFollows(User $user)
    {
        return $this->follows->contains($user);
    }
}
