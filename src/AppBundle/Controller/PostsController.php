<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\AddPostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class PostsController extends Controller
{
    /**
     * @Route("/")
     */
    public function mainPageAction()
    {

        if($this->getUser()){
            return $this->redirectToRoute("mainPage");
        }

        return $this->render('@App/Posts/index.html.twig', array(
            // ...
        ));
    }
    /**
     * @Route("/main", name="mainPage")
     */
    public function loadAllPostsAction()
    {

        $allPost = $this->getDoctrine()
                        ->getRepository('AppBundle:Post')
                        ->findAll();

        dump($this->getUser());
        return $this->render('@App/Posts/load_all_posts.html.twig', array(
            'all_post' => $allPost,
            'current_user' => $this->getUser()
        ));
    }

    /**
     * @Route("/add")
     * @param \Symfony\Component\HttpFoundation\Request
     */
    public function myProfileAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(AddPostType::class, $post);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $post->setAuthorUserId($this->getUser());
            $post = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            return $this->redirectToRoute("mainPage");
        }

        $currentUser = $this->getUser();



        return $this->render('@App/Posts/add_post.html.twig', array(
            'form' => $form->createView(),
//            'userPost' => $userPost,
            'currentUser' => $currentUser,
            'friendPost' => $currentUser->getFollows()
        ));
    }

    /**
     * @Route("/like/{id}", requirements={"id": "\d+"}, name="like")
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function addLike($id)
    {
        $currentPost = $this->getDoctrine()
                            ->getRepository('AppBundle:Post')
                            ->find($id);
        $currentUser = $this->getUser();


        $currentPost->addLikers($currentUser);

        $em = $this->getDoctrine()->getManager();
        $em->persist($currentPost);
        $em->flush();



        return $this->redirectToRoute("mainPage");
    }

    /**
     * @Route("/followed/{id}", requirements={"id": "\d+"}, name="followed")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function madeFollowers(User $user)
    {
//        $userToFollowed = $this->getDoctrine()
//                            ->getRepository('AppBundle:User')
//                            ->find($id);

        $userWhoWantFollowed = $this->getUser();

        $userWhoWantFollowed->addFollows($user);

        $em = $this->getDoctrine()->getManager();
        $em->persist($userWhoWantFollowed);
        $em->flush();

        return $this->redirectToRoute("mainPage");
    }

}
