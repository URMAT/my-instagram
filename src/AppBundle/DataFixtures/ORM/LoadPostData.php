<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Article;
use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPostData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setUsername('User 1')
            ->setEmail('test@gmail.com')
            ->setPassword('$2y$13$XDfVZonMOCw.wsrkSbbjcueJK8ptIU/gO2xeXHmUevN0dgRrgUUcC')
            ->setAuthor('User 1')
            ->setEnabled(1);

        $manager->persist($user);

        $user2 = new User();
        $user2
            ->setUsername('User 2')
            ->setEmail('test2@gmail.com')
            ->setPassword('$2y$13$rOBn8oIJkB.A3MEYQevEUeGWX06GwUXmK6wpUYM7JRjZT2ZxIs1Oa')
            ->setAuthor('User 2')
            ->setEnabled(1);

        $manager->persist($user2);

        $user3 = new User();
        $user3
            ->setUsername('User 3')
            ->setEmail('test3@gmail.com')
            ->setPassword('$2y$13$S/8mbKij1EYiqrPdvNTDru1mZBQ94lMx6korte8e3fDJ9.R0B6.xS')
            ->setAuthor('User 3')
            ->setEnabled(1);

        $manager->persist($user3);

        $user4 = new User();
        $user4
            ->setUsername('User 4')
            ->setEmail('test4@gmail.com')
            ->setPassword('$2y$13$4k7z2QNP0ZW04FtBO1oeVOjI8oenSixXgMO1EwsaZB4uYcaVA1ozq')
            ->setAuthor('User 4')
            ->setEnabled(1);

        $manager->persist($user4);

        $user5 = new User();
        $user5
            ->setUsername('User 5')
            ->setEmail('test5@gmail.com')
            ->setPassword('$2y$13$oVNBFNeb/mS70XSV16OTaex2tRkj/Q6OeIAU.PNzXDSyVApGdPbEq')
            ->setAuthor('User 5')
            ->setEnabled(1);

        $manager->persist($user5);


        $post = new Post();
        $post
            ->setImage('5b0a66f6c702d.jpg')
            ->setAuthorUserId(6)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post);

        $post1 = new Post();
        $post1
            ->setImage('5b0a672490465.jpg')
            ->setAuthorUserId(6)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post1);

        $post2 = new Post();
        $post2
            ->setImage('5b0a67493b9e0.jpg')
            ->setAuthorUserId(6)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post2);

        $post3 = new Post();
        $post3
            ->setImage('5b0a676938311.jpg')
            ->setAuthorUserId(6)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post3);

        $post4 = new Post();
        $post4
            ->setImage('5b0a68ce3f4bf.jpg')
            ->setAuthorUserId(7)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post4);

        $post5 = new Post();
        $post5
            ->setImage('5b0a69342ad74.jpg')
            ->setAuthorUserId(8)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post5);

        $post6 = new Post();
        $post6
            ->setImage('5b0a6941416a6.jpg')
            ->setAuthorUserId(8)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post6);

        $post7 = new Post();
        $post7
            ->setImage('5b0a6a1d77c44.jpg')
            ->setAuthorUserId(8)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post7);

        $post8 = new Post();
        $post8
            ->setImage('5b0a6a4020eb8.jpg')
            ->setAuthorUserId(7)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post8);

        $post9 = new Post();
        $post9
            ->setImage('5b0a6a63e0edf.jpg')
            ->setAuthorUserId(7)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post9);

        $post10 = new Post();
        $post10
            ->setImage('5b0a6a83e539f.jpg')
            ->setAuthorUserId(7)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post10);

        $post11 = new Post();
        $post11
            ->setImage('5b0a6af29537f.jpeg')
            ->setAuthorUserId(9)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post11);

        $post12 = new Post();
        $post12
            ->setImage('5b0a6b0382898.jpg')
            ->setAuthorUserId(9)
            ->setDescription('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.');

        $manager->persist($post12);


        $manager->flush();
    }
}
